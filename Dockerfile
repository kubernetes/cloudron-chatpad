# build stage
FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df AS build
RUN yarn add parcel
RUN mkdir -p /app/code
WORKDIR /app/code
ADD https://github.com/deiucanta/chatpad/archive/refs/heads/main.zip /app/code
RUN unzip main.zip && rm main.zip 
WORKDIR /app/code/chatpad-main
RUN npm install
RUN npm run build

# production stage
FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

COPY --from=build /app/code/chatpad-main/dist /app/code/html

RUN yarn global add http-server
COPY start.sh /app/code/

CMD [ "/app/code/start.sh" ]
