#!/bin/bash

set -eu

chown -R cloudron:cloudron /app/data

echo "==> Starting ChatPad"
exec /usr/local/bin/gosu cloudron:cloudron http-server /app/code/html -a 0.0.0.0 --port 3000 -d false
