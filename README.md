## THIS IS USING THE ORIGINAL PROJECT TO MAKE IT POSSIBLE TO DEPLOY IT IN CLOUDRON 

# ChatPad
Package to deploy ChatPad to Cloudron

The original ChatPad Source can be found in [Github](https://github.com/deiucanta/chatpad)

Required Changes are:
- Adding CloudronManifest.json
- Adding nginx Configuration to configure use with Read-Only environment

To deploy it manually on Cloudron follow this instruction:

Build the docker Image:
~~~
docker build -t APPNAME .
~~~

Tag the docker Image:
~~~
docker tag APPNAME:latest URL-OF-YOUR-IMAGE-REGISTRY/APPNAME
~~~

Push Image to registry:
~~~
docker push URL-OF-YOUR-IMAGE-REGISTRY/APPNAME
~~~

Install image on your Cloudron Instance:
~~~
cloudron install --image URL-OF-YOUR-IMAGE-REGISTRY/APPNAME
~~~
